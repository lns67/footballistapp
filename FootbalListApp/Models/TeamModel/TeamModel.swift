//
//  TeamModel.swift
//  FootbalListApp
//
//  Created by DS on 31.10.2021.
//

import Foundation

// MARK: - TeamModel

struct TeamResponseModel: Codable {
    let response: [TeamModel]
}

struct TeamModel: Codable {
    let team: Info
    let venue: Venue
}

struct Info: Codable {
    let id: Int
    let name: String
    let country: String
    let founded: Int
    let national: Bool
    let logo: String
}

struct Venue: Codable {
    let id: Int
    let name: String
    let address: String
    let city: String
    let capacity: Int
    let surface: String
    let image: String
}
