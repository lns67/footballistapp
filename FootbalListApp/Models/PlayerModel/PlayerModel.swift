//
//  PlayerModel.swift
//  FootbalListApp
//
//  Created by DS on 31.10.2021.
//

import Foundation
import UIKit

// MARK: - PlayerModel

struct PlayerResponseModel: Codable {
    let response: [PlayerGlobal]
}

struct PlayerGlobal: Codable {
    let player: Player
    let statistics: [Statistics]
}

struct Player: Codable {
    let age: Int
    let birth: Birthday
    let firstname:String
    let height: String
    let id: Int
    let injured: Bool
    let lastname: String
    let name: String
    let nationality: String
    let photo: String
    let weight: String
}

struct Statistics: Codable {
    let team: Team
}

struct Team: Codable {
    let id: Int
    let logo: String
    let name: String
}

struct Birthday: Codable {
    let country: String
    let date: String
    let place: String
}
