//
//  NetworkManager.swift
//  FootbalListApp
//
//  Created by DS on 31.10.2021.
//

import Foundation

// MARK: - NetworkManager

class NetworkManager {
    
    static let shared = NetworkManager()
    let playersUrlPath = "https://api-football-beta.p.rapidapi.com/players/topscorers"
    let teamsUrlPath = "https://api-football-beta.p.rapidapi.com/teams"
    let apiHost = "api-football-beta.p.rapidapi.com"
    let apiKey = "078330417bmsh94a5cd2f513d694p1616e8jsn59bf695086c9"
    
    func getPlayers(league: String, season: String, completionHandler: @escaping ([PlayerGlobal]?, String?) -> Void) {
        
        let urlComponents = URLComponents(string: playersUrlPath)
        
        guard var urlComponents = urlComponents else { return }
        
        let queryItemLeague = URLQueryItem(name: "league", value: league)
        let queryItemSeason = URLQueryItem(name: "season", value: season)
        
        urlComponents.queryItems = [queryItemLeague, queryItemSeason]
        
        guard let url = urlComponents.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["X-RapidAPI-Host" : apiHost,
                                       "X-RapidAPI-Key" : apiKey]
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                completionHandler(nil, error?.localizedDescription)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(PlayerResponseModel.self, from: data)
                completionHandler(response.response, nil)
            
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
        }.resume()
    }
    
    func getTeamByID(team_id: Int, completionHandler: @escaping (TeamModel?, String?) -> Void) {
        
        let urlComponents = URLComponents(string: teamsUrlPath)
        
        guard var urlComponents = urlComponents else { return }
        
        let queryItemTeamID = URLQueryItem(name: "id", value: String(team_id))
        
        urlComponents.queryItems = [queryItemTeamID]
        
        guard let url = urlComponents.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["X-RapidAPI-Host" : apiHost,
                                       "X-RapidAPI-Key" : apiKey]
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                completionHandler(nil, error?.localizedDescription)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(TeamResponseModel.self, from: data)
                
                completionHandler(response.response[0], nil)
            
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
        }.resume()
    }
}
