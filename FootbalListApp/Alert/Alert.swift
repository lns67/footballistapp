//
//  Alert.swift
//  FootbalListApp
//
//  Created by DS on 31.10.2021.
//

import Foundation
import UIKit

// MARK: - Alert

class Alert {
    
    func showAlert(title: String? = nil, message: String? = nil, buttonTitle: String? = nil) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: buttonTitle, style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        return alert
    }
}
