//
//  EntereViewController.swift
//  FootbalListApp
//
//  Created by DS on 31.10.2021.
//

import UIKit

// MARK: - EnterViewController
class EnterViewController: UIViewController {

    @IBOutlet weak var leagueNumberTextField: UITextField!
    @IBOutlet weak var seasonTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onShowButton(_ sender: Any) {
        
        guard
            let league = leagueNumberTextField.text, let season = seasonTextField.text, !league.isEmpty, !season.isEmpty, (Int(league) != nil), (Int(season) != nil) else {
            self.present(Alert().showAlert(title: "Error", message: "All fields have to be entered\nCheck entered text", buttonTitle: "cancel"), animated: true, completion: nil)
            return
        }
        
        let playersListVC = PlayersListViewController(nibName: "PlayersListViewController", bundle: nil)
        playersListVC.league = league
        playersListVC.season = season
        navigationController?.pushViewController(playersListVC, animated: true)
    }
}
