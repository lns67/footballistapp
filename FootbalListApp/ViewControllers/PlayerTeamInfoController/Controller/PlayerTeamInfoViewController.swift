//
//  PlayerTeamInfoViewController.swift
//  FootbalListApp
//
//  Created by DS on 31.10.2021.
//

import UIKit

// MARK: - PlayerTeamInfoViewController
class PlayerTeamInfoViewController: UIViewController {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    var player: PlayerGlobal?
    var team: TeamModel?
    var team_id: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let player = player else {
            return
        }
        
        nameLabel.text = player.player.name
        infoLabel.text = "Age: \(player.player.age)\nHeight: \(player.player.height)\nWeight: \(player.player.weight)\nNationality: \(player.player.nationality)\nInjured: \(player.player.injured)"
        if let imageURL = URL(string: player.player.photo) {
            loadImage(url: imageURL)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let team_id = team_id else {
            return
        }
        
        NetworkManager().getTeamByID(team_id: team_id) { team, error in
            
            guard error == nil else {
                self.present(Alert().showAlert(title: "Error", message: error, buttonTitle: "cancel"), animated: true, completion: nil)
                return
            }
            
            guard let team = team else {
                return
            }
            
            self.team = team
            
            DispatchQueue.main.async {
                
                self.nameLabel.text = team.team.name
                self.infoLabel.text = "Country: \(team.team.country)\nFounded: \(team.team.founded)\nStadium: \(team.venue.name)\nAddress: \(team.venue.address)"
                if let imageURL = URL(string: team.team.logo) {
                    self.loadImage(url: imageURL)
                }
                
            }
        }
    }
    
    @IBAction func onCancelButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension PlayerTeamInfoViewController {
    
    private func loadImage(url: URL) {
        
        DispatchQueue.global().async {
            
            if let data = try? Data(contentsOf: url) {
                
                if let image = UIImage(data: data) {
                    
                    DispatchQueue.main.async { [self] in
                        
                        mainImage.image = image
                    }
                }
            }
        }
    }
}
