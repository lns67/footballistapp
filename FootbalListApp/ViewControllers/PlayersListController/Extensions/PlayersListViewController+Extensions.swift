//
//  PlayersListViewController+Extensions.swift
//  FootbalListApp
//
//  Created by DS on 31.10.2021.
//

import Foundation
import UIKit

// MARK: - PlayersListViewController + Extensions

// MARK: - UITableViewDelegate, UITableViewDataSource
extension PlayersListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: playersListCellID, for: indexPath) as! PlayersListCell
        cell.update(playerGlobal: playersList[indexPath.row])
        cell.tag = indexPath.row
        cell.delegate = self
        return cell
    }
}

extension PlayersListViewController: PlayersListCellDelegate {
    
    func onFullnameLabelSelected(index: Int) {
        
        let playerTeamInfoVC = PlayerTeamInfoViewController(nibName: "PlayerTeamInfoViewController", bundle: nil)
        playerTeamInfoVC.player = playersList[index]
        navigationController?.pushViewController(playerTeamInfoVC, animated: true)
    }
    
    func onTeamLabelSelected(index: Int) {
        
        let playerTeamInfoVC = PlayerTeamInfoViewController(nibName: "PlayerTeamInfoViewController", bundle: nil)
        playerTeamInfoVC.team_id = playersList[index].statistics[0].team.id
        navigationController?.pushViewController(playerTeamInfoVC, animated: true)
    }
}
