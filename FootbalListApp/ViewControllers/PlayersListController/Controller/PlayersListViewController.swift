//
//  PlayersListViewController.swift
//  FootbalListApp
//
//  Created by DS on 31.10.2021.
//

import UIKit


// MARK: - PlayersListViewController
class PlayersListViewController: UIViewController {

    @IBOutlet weak var playersTableView: UITableView!
    
    var playersList: [PlayerGlobal] = []
    
    var league: String = ""
    var season: String = ""
    let playersListCellID = "PlayersListCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playersTableView.register(UINib(nibName: playersListCellID, bundle: nil), forCellReuseIdentifier: playersListCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NetworkManager().getPlayers(league: league, season: season) { players, error in
            
            guard error == nil else {
                self.present(Alert().showAlert(title: "Error", message: error, buttonTitle: "cancel"), animated: true, completion: nil)
                return
            }

            guard let players = players else {
                self.present(Alert().showAlert(title: "Error", message: error, buttonTitle: "cancel"), animated: true, completion: nil)
                return
            }
            
            self.playersList = players
            
            DispatchQueue.main.async {
                self.playersTableView.reloadData()
            }
        }
    }
    
    @IBAction func onCancelButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
