//
//  PlayersListCell.swift
//  FootbalListApp
//
//  Created by DS on 31.10.2021.
//

import UIKit

protocol PlayersListCellDelegate {
    func onFullnameLabelSelected(index: Int)
    func onTeamLabelSelected(index: Int)
}

// MARK: - PlayersListCell
class PlayersListCell: UITableViewCell {

    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var teamLabel: UILabel!
    
    var delegate: PlayersListCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func update(playerGlobal: PlayerGlobal){
        fullnameLabel.text = playerGlobal.player.name
        teamLabel.text = playerGlobal.statistics[0].team.name
    }
    
    @IBAction func onFullnameLabel(_ sender: Any) {
        delegate?.onFullnameLabelSelected(index: self.tag)
    }
    
    @IBAction func onTeamLabel(_ sender: Any) {
        delegate?.onTeamLabelSelected(index: self.tag)
    }
}
